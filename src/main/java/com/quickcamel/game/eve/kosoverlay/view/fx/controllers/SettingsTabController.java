/*
 * Copyright (c) 2014.
 * Author: Louis Burton
 * Hosted: https://bitbucket.org/louisburton/kosoverlay
 *
 * This file is part of KOS Overlay.
 *
 *     KOS Overlay is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     KOS Overlay is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with KOS Overlay.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.quickcamel.game.eve.kosoverlay.view.fx.controllers;

import com.quickcamel.game.eve.kosoverlay.configuration.IConfigManager;

import javax.inject.Inject;

/**
 * Abstract Settings tab functionality
 *
 * @author Louis Burton
 */
public abstract class SettingsTabController implements ISettingsTabController {

    @Inject
    protected IConfigManager m_configManager;

    public boolean updateValueIfSet(String key, String value) {
        boolean changed = false;
        if (value != null) {
            String trimmedValue = value.trim();
            if (trimmedValue.length() > 0) {
                if (!trimmedValue.equals(m_configManager.getValue(key))) {
                    changed = true;
                    m_configManager.setValue(key, trimmedValue);
                }
            }
        }
        return changed;
    }

    public boolean updateValueIfChanged(String key, String value) {
        boolean changed = false;
        String trimmedValue;
        if (value == null) {
            trimmedValue = "";
        }
        else {
            trimmedValue = value.trim();
        }
        if (!trimmedValue.equals(m_configManager.getValue(key, ""))) {
            changed = true;
            m_configManager.setValue(key, trimmedValue);
        }
        return changed;
    }
}
