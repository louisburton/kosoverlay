/*
 * Copyright (c) 2014.
 * Author: Louis Burton
 * Hosted: https://bitbucket.org/louisburton/kosoverlay
 *
 * This file is part of KOS Overlay.
 *
 *     KOS Overlay is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     KOS Overlay is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with KOS Overlay.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.quickcamel.game.eve.kosoverlay.view.fx.controllers;

import com.quickcamel.game.eve.kosoverlay.configuration.ConfigKeyConstants;
import com.quickcamel.game.eve.kosoverlay.configuration.IConfigChangeListener;
import com.quickcamel.game.eve.kosoverlay.view.AlwaysOnTopDialogue;
import javafx.fxml.FXML;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.media.AudioClip;
import javafx.scene.media.MediaException;
import javafx.stage.FileChooser;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;
import javax.inject.Inject;
import java.io.File;

/**
 * Controller functionality for the settings tab - orientated around sound and killboard considerations
 *
 * @author Louis Burton
 */
public class SettingsTabSoundsKBController extends SettingsTabController {

    private static final Logger m_logger = LoggerFactory.getLogger(SettingsTabSoundsKBController.class);

    public static final String PLAY_KOS_SOUND_TOOLTIP = "If you want to hear when a KOS result is displayed.";
    public static final String CUSTOM_KOS_SOUND_TOOLTIP = "If you want to hear your own custom sound when a KOS result is displayed.\n" +
            "Clear this field to restore the default KOS sound.";
    public static final String PLAY_KOS_SOUND_ONCE_TOOLTIP = "If you want to hear only the first KOS result of a large batch.\n" +
            "Small batches will always try to play the sound for every KOS result.";
    public static final String INFO_URL_TOOLTIP = "The link provided in the expandable result information.\n" +
            "Can replace <eveid> and <name>";
    public static final String INFO_URL_EVEKILL_TOOLTIP = "If you are using an Eve-kill based board, which requires an initial POST search (i.e. your URL looks similar to 'http://eve-kill.net/?a=search').\n" +
            "These links will take slightly longer to appear, and the search can't be avoided due to their use of KB specific Ids.";
    public static final String INFO_URL_CURRENT_MONTH_LOSSES_TOOLTIP = "If you want to be taken to the current month's losses page.\n" +
            "Not needed on some boards (i.e. the sev3rance KB), but eve-kill will take you to the all time summary otherwise.";

    @FXML
    private CheckBox m_playKosSound;

    @FXML
    private TextField m_pilotInfoURL;

    @FXML
    private CheckBox m_pilotInfoPostNeeded;

    @FXML
    private CheckBox m_eveKill;

    @FXML
    private TextField m_kosSoundPath;

    @FXML
    private Button m_kosSoundButton;

    @FXML
    private CheckBox m_playKosSoundOnce;

    @FXML
    private Parent m_root;

    private FileChooser m_kosSoundChooser;

    @Inject
    private AlwaysOnTopDialogue m_alwaysOnTopDialogue;

    @Resource(name = "kosBatchService")
    private IConfigChangeListener m_batchServiceConfigListener;

    @FXML
    public void initialize() {
        m_logger.debug("Initialising");
        m_kosSoundChooser = new FileChooser();
        m_kosSoundChooser.setTitle("Choose KOS Sound");
        m_playKosSound.setSelected(Boolean.valueOf(m_configManager.getValue(ConfigKeyConstants.PLAY_KOS_SOUND, ConfigKeyConstants.PLAY_KOS_SOUND_DEFAULT)));
        m_kosSoundPath.setText(m_configManager.getValue(ConfigKeyConstants.CUSTOM_KOS_SOUND));
        m_playKosSoundOnce.setSelected(Boolean.valueOf(m_configManager.getValue(ConfigKeyConstants.PLAY_KOS_SOUND_ONCE, ConfigKeyConstants.PLAY_KOS_SOUND_ONCE_DEFAULT)));
        m_pilotInfoURL.setText(m_configManager.getValue(ConfigKeyConstants.PILOTINFO_URL, ConfigKeyConstants.PILOTINFO_URL_DEFAULT));
        m_pilotInfoPostNeeded.setSelected(Boolean.valueOf(m_configManager.getValue(ConfigKeyConstants.PILOTINFO_URL_POST_NEEDED, ConfigKeyConstants.PILOTINFO_URL_POST_NEEDED_DEFAULT)));
        m_eveKill.setSelected(Boolean.valueOf(m_configManager.getValue(ConfigKeyConstants.PILOTINFO_URL_POST_EVEKILL, ConfigKeyConstants.PILOTINFO_URL_POST_EVEKILL_DEFAULT)));
    }

    public void updateEveKillCheckBox() {
        m_eveKill.setDisable(!m_pilotInfoPostNeeded.isSelected());
    }

    public void updateSoundControls() {
        m_kosSoundPath.setDisable(!m_playKosSound.isSelected());
        m_playKosSoundOnce.setDisable(!m_playKosSound.isSelected());
        m_kosSoundButton.setDisable(!m_playKosSound.isSelected());
    }

    public void openFileChooser() {
        m_alwaysOnTopDialogue.setDialogOpen(true);
        File file = m_kosSoundChooser.showOpenDialog(m_root.getScene().getWindow());
        if (file != null) {
            m_kosSoundPath.setText(file.getAbsolutePath());
        }
        m_alwaysOnTopDialogue.setDialogOpen(false);
    }

    public String validate() {
        String errorText = null;
        if (!StringUtils.isEmpty(m_kosSoundPath.getText())) {
            try {
                new AudioClip(new File(m_kosSoundPath.getText()).toURI().toASCIIString());
            }
            catch (MediaException e) {
                errorText = "The audio file provided is unsupported.";
            }
        }
        return errorText;
    }

    public void save() {
        m_logger.debug("Saving settings");
        updateValueIfSet(ConfigKeyConstants.PLAY_KOS_SOUND, String.valueOf(m_playKosSound.isSelected()));
        boolean needsBatchServiceUpdate = updateValueIfChanged(ConfigKeyConstants.CUSTOM_KOS_SOUND, m_kosSoundPath.getText());
        updateValueIfSet(ConfigKeyConstants.PLAY_KOS_SOUND_ONCE, String.valueOf(m_playKosSoundOnce.isSelected()));
        updateValueIfSet(ConfigKeyConstants.PILOTINFO_URL, m_pilotInfoURL.getText());
        updateValueIfSet(ConfigKeyConstants.PILOTINFO_URL_POST_NEEDED, String.valueOf(m_pilotInfoPostNeeded.isSelected()));
        updateValueIfSet(ConfigKeyConstants.PILOTINFO_URL_POST_EVEKILL, String.valueOf(m_eveKill.isSelected()));

        if (needsBatchServiceUpdate) {
            m_batchServiceConfigListener.notifyConfigChange();
        }
    }
}