/*
 * Copyright (c) 2014.
 * Author: Louis Burton
 * Hosted: https://bitbucket.org/louisburton/kosoverlay
 *
 * This file is part of KOS Overlay.
 *
 *     KOS Overlay is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     KOS Overlay is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with KOS Overlay.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.quickcamel.game.eve.kosoverlay.service;

import com.quickcamel.game.eve.kosoverlay.configuration.ConfigKeyConstants;
import com.quickcamel.game.eve.kosoverlay.configuration.IConfigManager;
import com.quickcamel.game.eve.kosoverlay.service.rest.IRestClient;
import com.quickcamel.game.eve.kosoverlay.service.dto.KOSPilotResultDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Description: Targets boards like eve-kill that require a search POST to get their custom ids
 *
 * @author Louis Burton
 */
public class PilotInfoURLBuilder implements IPilotInfoURLBuilder {

    private static final Logger m_logger = LoggerFactory.getLogger(PilotInfoURLBuilder.class);

    @Inject
    private IConfigManager m_configManager;

    @Inject
    private IRestClient m_restClient;

    public URI getURL(final KOSPilotResultDTO pilot) {
        URI result = null;
        try {
            String url = m_configManager.getValue(ConfigKeyConstants.PILOTINFO_URL, ConfigKeyConstants.PILOTINFO_URL_DEFAULT);
            url = replacePlaceHolders(url, pilot);

            if (!Boolean.valueOf(m_configManager.getValue(ConfigKeyConstants.PILOTINFO_URL_POST_NEEDED, ConfigKeyConstants.PILOTINFO_URL_POST_NEEDED_DEFAULT))) {
                result = new URI(url);
            }
            else {
                String postParams = m_configManager.getValue(ConfigKeyConstants.PILOTINFO_URL_POST_PARAMS, "searchphrase:<name>¦searchtype:pilot");
                String[] postPairs = postParams.split("¦");
                Map<String, String> formParams = new HashMap<>();
                for (String postPair : postPairs) {
                    String[] keyValue = postPair.split(":");
                    formParams.put(keyValue[0], replacePlaceHolders(keyValue[1], pilot));
                }
                result = m_restClient.post(url, formParams);
                if (result != null && Boolean.valueOf(m_configManager.getValue(ConfigKeyConstants.PILOTINFO_URL_POST_EVEKILL, ConfigKeyConstants.PILOTINFO_URL_POST_EVEKILL_DEFAULT))) {
                    final Date now = new Date();
                    result = new URI(result.toString() + "&view=losses&m=" + new SimpleDateFormat("MM").format(now) + "&y=" + new SimpleDateFormat("yyyy").format(now));
                }
            }
            if (m_logger.isDebugEnabled()) {
                m_logger.debug("URI = " + (result == null ? "null" : result.toString()));
            }
        }
        catch (Exception e) {
            m_logger.error("Unable to create URI : " + result, e);
        }
        return result;
    }

    private String replacePlaceHolders(String originalString,
                                       KOSPilotResultDTO pilot) throws UnsupportedEncodingException {
        if (originalString != null) {
            originalString = originalString.replace("<name>", String.valueOf(pilot.getLabel()));
            originalString = originalString.replace("<eveid>", String.valueOf(pilot.getEveId()));
        }
        return originalString;
    }
}
