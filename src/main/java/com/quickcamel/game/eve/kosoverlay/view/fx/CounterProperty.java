/*
 * Copyright (c) 2014.
 * Author: Louis Burton
 * Hosted: https://bitbucket.org/louisburton/kosoverlay
 *
 * This file is part of KOS Overlay.
 *
 *     KOS Overlay is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     KOS Overlay is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with KOS Overlay.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.quickcamel.game.eve.kosoverlay.view.fx;

import javafx.beans.property.IntegerPropertyBase;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Description: Thread safe counter for a JavaFX property
 *
 * @author Louis Burton
 */
public class CounterProperty extends IntegerPropertyBase {

    private AtomicInteger m_counter = new AtomicInteger(0);

    @Override
    public Object getBean() {
        return m_counter;
    }

    @Override
    public String getName() {
        return "counter";
    }

    @Override
    public int get() {
        return m_counter.get();
    }

    @Override
    public void set(int newValue) {
        m_counter.set(newValue);
        super.fireValueChangedEvent();
    }

    public void increment() {
        m_counter.incrementAndGet();
        super.fireValueChangedEvent();
    }
}
