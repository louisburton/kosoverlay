/*
 * Copyright (c) 2016.
 * Author: Louis Burton
 * Hosted: https://bitbucket.org/louisburton/kosoverlay
 *
 * This file is part of KOS Overlay.
 *
 *     KOS Overlay is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     KOS Overlay is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with KOS Overlay.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.quickcamel.game.eve.kosoverlay.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.WebApplicationException;

/**
 * Description: Handle CVA API WebApplicationExceptions
 *
 * @author Louis Burton
 */
public class CVARESTLogger {

    private static final Logger m_logger = LoggerFactory.getLogger(CVARESTLogger.class);

    public static void handleWebApplicationException(String context, WebApplicationException e) {
        if (e.getResponse().getStatus() == 429) {
            m_logger.error("HTTP 429 - CVA Throttled Request - {}", context);
        }
    }
}
